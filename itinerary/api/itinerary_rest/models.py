from django.db import models
from django.urls import reverse


class Trip(models.Model):
    trip_name = models.CharField(max_length=100)
    destination = models.CharField(max_length=100)
    description = models.CharField(max_length=300, null=True)
    start_date = models.DateField()
    end_date = models.DateField()

    def get_api_url(self):
        return reverse("api_trip", kwargs={"pk": self.id})


class ItineraryDate(models.Model):
    date = models.DateField()
    note = models.CharField(max_length=100, null=True)

    def get_api_url(self):
        return reverse("api_itinerary_date", kwargs={"pk": self.id})


class Activity(models.Model):
    activity_date = models.ForeignKey(
        ItineraryDate,
        related_name="activity_date",
        on_delete=models.CASCADE,
    )
    activity = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    duration = models.CharField(max_length=20)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    location = models.CharField(max_length=100)
    trip = models.ForeignKey(
        Trip,
        related_name="trip",
        on_delete=models.CASCADE,
    )

    class Reservation(models.TextChoices):
        REQUIRED = "REQUIRED"
        NOT_REQUIRED = "NOT_REQUIRED"
        RECOMMENDED = "RECOMMENDED"

    reservation = models.CharField(
        max_length=20,
        choices=Reservation.choices,
        default=Reservation.NOT_REQUIRED,
    )

    class Status(models.TextChoices):
        PENDING = "PENDING"
        SCHEDULED = "SCHEDULED"

    status = models.CharField(
        max_length=20,
        choices=Status.choices,
        default=Status.PENDING,
    )

    time = models.TimeField(null=True)

    def get_api_url(self):
        return reverse("api_activity", kwargs={"pk": self.id})
