from django.urls import path
from .views import (
    api_trips,
    api_trip,
    api_itinerary_dates,
    api_itinerary_date,
    api_activities,
    api_activity,
    api_health_check,
)

urlpatterns = [
    path("trips/", api_trips, name="trips"),
    path("trips/<int:pk>/", api_trip, name="show_trip"),
    path("dates/", api_itinerary_dates, name="itinerary_dates"),
    path("dates/<int:pk>/", api_itinerary_date, name="show_date"),
    path("activities/", api_activities, name="activities"),
    path("activities/<int:pk>", api_activity, name="show_activity"),
    path("health-check/", api_health_check, name="health_check"),
]
