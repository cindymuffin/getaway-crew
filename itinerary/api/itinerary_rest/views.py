from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Trip, ItineraryDate, Activity


class TripEncoder(ModelEncoder):
    model = Trip
    properties = [
        "id",
        "trip_name",
        "destination",
        "description",
        "start_date",
        "end_date",
    ]


class ItineraryDateEncoder(ModelEncoder):
    model = ItineraryDate
    properties = ["id", "date", "note"]


class ActivityEncoder(ModelEncoder):
    model = Activity
    properties = [
        "id",
        "activity_date",
        "activity",
        "description",
        "duration",
        "price",
        "location",
        "reservation",
        "status",
        "trip",
    ]
    encoders = {
        "activity_date": ItineraryDateEncoder(),
        "trip": TripEncoder(),
    }

    def get_extra_data(self, o):
        return {"price": float(o.price)}


@require_http_methods(["GET", "POST"])
def api_trips(request):
    print("hello")
    if request.method == "GET":
        trips = Trip.objects.all()
        return JsonResponse({"trips": trips}, encoder=TripEncoder)
    elif request.method == "POST":
        print("hi")
        content = json.loads(request.body)
        try:
            trip = Trip.objects.create(**content)
            return JsonResponse(trip, encoder=TripEncoder, safe=False)
        except Exception as e:
            return JsonResponse({"message": type(e)}, status=500)
    else:
        return JsonResponse(
            {"message": "Request method does not exist"}, status=400
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_trip(request, pk):
    if request.method == "GET":
        try:
            trip = Trip.objects.get(id=pk)
            return JsonResponse(trip, encoder=TripEncoder, safe=False)
        except Trip.DoesNotExist:
            return JsonResponse(
                {"message": "Trip trip does not exist"}, status=404
            )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            trip = Trip.objects.get(id=pk)
            props = [
                "trip_name",
                "destination",
                "description",
                "start_date",
                "end_date",
            ]
            for prop in props:
                if prop in content:
                    setattr(trip, prop, content[prop])
            trip.save()
            return JsonResponse(trip, encoder=TripEncoder, safe=False)
        except Trip.DoesNotExist:
            return JsonResponse(
                {"message": "Trip trip does not exist"}, status=404
            )
    elif request.method == "DELETE":
        count, _ = Trip.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        return JsonResponse(
            {"message": "Request method does not exist"}, status=400
        )


@require_http_methods(["GET", "POST"])
def api_itinerary_dates(request):
    if request.method == "GET":
        dates = ItineraryDate.objects.all()
        return JsonResponse({"dates": dates}, encoder=ItineraryDateEncoder)
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            date = ItineraryDate.objects.create(**content)
            return JsonResponse(date, encoder=ItineraryDateEncoder, safe=False)
        except Exception as e:
            return JsonResponse({"message": type(e)}, status=500)
    else:
        return JsonResponse(
            {"message": "Request method does not exist"}, status=400
        )


@require_http_methods(["GET", "DELETE"])
def api_itinerary_date(request, pk):
    if request.method == "GET":
        try:
            date = ItineraryDate.objects.get(id=pk)
            return JsonResponse(date, encoder=ItineraryDateEncoder, safe=False)
        except ItineraryDate.DoesNotExist:
            return JsonResponse(
                {"message": "Itinerary date does not exist"}, status=404
            )
    elif request.method == "DELETE":
        count, _ = ItineraryDate.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        return JsonResponse(
            {"message": "Request method does not exist"}, status=400
        )


@require_http_methods(["GET", "POST"])
def api_activities(request):
    if request.method == "GET":
        filters = {}
        search_status = request.GET.get("status")
        if search_status:
            filters["status"] = search_status
        activities = Activity.objects.filter(**filters)
        return JsonResponse(
            {"activities": activities}, encoder=ActivityEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            date_id = content["activity_date"]
            activity_date = ItineraryDate.objects.get(id=date_id)
            content["activity_date"] = activity_date
        except ItineraryDate.DoesNotExist:
            return JsonResponse(
                {"message": "Itinerary date does not exist"}, status=404
            )
        try:
            trip_id = content["trip"]
            trip = Trip.objects.get(id=trip_id)
            content["trip"] = trip
        except Trip.DoesNotExist:
            return JsonResponse({"message": "Trip does not exist"}, status=404)
        print(content)
        activity = Activity.objects.create(**content)
        return JsonResponse(activity, encoder=ActivityEncoder, safe=False)
    else:
        return JsonResponse(
            {"message": "Request method does not exist"}, status=400
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_activity(request, pk):
    if request.method == "GET":
        try:
            activity = Activity.objects.get(id=pk)
            return JsonResponse(activity, encoder=ActivityEncoder, safe=False)
        except Activity.DoesNotExist:
            return JsonResponse(
                {"message": "Activity does not exist"}, status=404
            )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            activity = Activity.objects.get(id=pk)
            props = [
                "activity_date",
                "activity",
                "description",
                "duration",
                "price",
                "location",
                "reservation",
            ]
            for prop in props:
                if prop in content:
                    setattr(activity, prop, content[prop])
            activity.save()
            return JsonResponse(activity, encoder=ActivityEncoder, safe=False)
        except Activity.DoesNotExist:
            return JsonResponse(
                {"message": "Activity does not exist"}, status=404
            )
    elif request.method == "DELETE":
        count, _ = Activity.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        return JsonResponse(
            {"message": "Request method does not exist"}, status=400
        )


@require_http_methods(["GET"])
def api_health_check(request):
    if request.method == "GET":
        return JsonResponse(
            {
                "message": "It works :) <3",
            },
            status=200,
        )
